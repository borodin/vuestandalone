const path = require('path');

module.exports = {
    entry: './src/init.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },
    module: {
        // module.rules é o mesmo que module.loaders em 1.x
        rules : [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    // `loaders` substituirá os carregadores padrões.
                    // A configuração a seguir fará com que todas as tags <script> sem
                    // o atributo "lang" sejam carrega com coffee-loader


                    postLoaders: {
                        html: 'babel-loader'
                    }


                }
            }
        ]
    }
};
