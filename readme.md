Vue Standalone
==============

Projeto simples com configurações enxutas para iniciar com vue em qualquer projeto não iniciado com vue-cli.


Para uso deve-se colocar seu código em src, e usar o webpack ```npm run build``` para criar o código pronto pra uso, e ```npm run watch``` para que o webpack possa assistir e compilar seu código a cada alteração. 

Ele vai ler o arquivo de configuração e vai jogar todo o conteúdo transcompilado para a pasta dist, bastando usar o arquivo final gerado lá.